export default user=>({
    id:user._id,
    firstName:user.firstName,
    lastName:user.lastName,
    email:user.email,
    city:user.city,
    status:user.status,
    gender:user.gender,
    created:user.createdAt,
    updated:user.updatedAt
})