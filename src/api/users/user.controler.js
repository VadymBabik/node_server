import {CreateUser} from "../../services/user.services"
import UserMapper from './user.mapper'

export const CreateUserController = async(request, response) => {
  const { firstName,
    lastName,
    password,
    email,
    salt,
    city,
    gender} = request.body
  const {user}= await CreateUser({firstName,
    lastName,
    password,
    email,
    salt,
    city,
    gender})
  response.status(201).json({
    success: true,
    data: UserMapper(user),
  });
};
