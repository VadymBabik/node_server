import { CreateUserController } from "./user.controler";
import { Router } from "express";
import User from "../../model/user.model";

const route = Router();
export default function (root) {
  root.use("/users", route);
  route.post("/register", CreateUserController);
}
