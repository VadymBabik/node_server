export const IndexController = (request, response) => {
  response.status(200).json({
    success: true,
    data: ["Hello index controller"],
  });
};
