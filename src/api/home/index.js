import { IndexController } from "./controller/index.controller";
import { Router } from "express";

const route = Router();
export default function (root) {
  root.use("/", route);
  route.get("/", IndexController);
}
