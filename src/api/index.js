import { Router } from "express";
import HomeRout from "../api/home";
import ProductRout from "../api/products";
import UserRout from "../api/users";
export default () => {
  const router = Router();
  HomeRout(router);
  ProductRout(router);
  UserRout(router);
  return router;
};
