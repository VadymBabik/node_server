import { ProductListController } from "./products.controler";
import { Router } from "express";

const route = Router();
export default function (root) {
  root.use("/products", route);
  route.get("/", ProductListController);
}
