import {randomBytes} from 'crypto';
import UserModel from '../model/user.model'
import argon2 from "argon2";

export const CreateUser=async ({email, lastName, firstName,city,password:pswdIn,gender})=>{
const salt=randomBytes(32)
const password=await argon2.hash(pswdIn, {salt})
const user=await UserModel.create({
    firstName,
    lastName,
    password,
    email,
    salt,
    city,
    gender
    });
return{
    user
    }
}