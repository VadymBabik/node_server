import mongoose from "mongoose";

const User = new mongoose.Schema({
    email:{type:String, unique:true, required:true},
    firstName:{type:String, required:true},
    lastName:{type:String, required:false},
    city:{type:String, required:false, default:null},
    salt:{type:String, required:true},
    password:{type:String, required:true},
    allowNewsLetters:{type:Boolean, default:true},
    termsAccepted:{type:Boolean, default:false},
    gender:{
        type:String,
        enum:['male', 'female', ''],
        default:'',
        required:false
    },
    status:{type:String,default:'1'},
    createdAt:{type: Date,default:new Date()},
    updatedAt:{type: Date,default:new Date()}
});

export default mongoose.model('User', User)