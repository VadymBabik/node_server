import mongoose from "mongoose";

const Product = new mongoose.Schema({
    title:{type:String, required:true},
    price:{type:mongoose.Decimal128, default:0},
    description:{type:String},
    status:{type:String,default:'1'},
    createdAt:{type: Date,default:new Date()},
    updatedAt:{type: Date,default:new Date()}
});

export default mongoose.model('Product', Product)