import express from "express";
import ApiRoutes from "../api";
import config from "../config";
import bodyParser from "body-parser";
import cors from "cors";

const allowedOrigins = [
  "http://localhost:3000",
  "http://localhost:8080",
  "https://node-expres-server.herokuapp.com",
  "https://my-lovely-site.com",
];

export default ({ config }) => {
  const app = express();
  app.use(
    cors({
      origin: function (origin, callback) {
        if (!origin) {
          return callback(null, true);
        }
        if (allowedOrigins.indexOf(origin) === -1) {
          const msg =
            "The CORS policy for this site does not allow access from the specified Origin.";
          return callback(new Error(msg), false);
        }
        return callback(null, true);
      },
      credentials: true,
    })
  );
  app.use(bodyParser.json())

  app.use(`/api/v${config.VERSION}`, ApiRoutes());
  return app;
};